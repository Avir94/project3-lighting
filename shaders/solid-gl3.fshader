#version 130

uniform vec3 uColor;

out vec4 fragColor;

void main() {
  if (uColor.x == -1)
    fragColor = vec4(uColor, 0.8);
  else
    fragColor = vec4(uColor, 1.0);
}
