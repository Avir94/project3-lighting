#version 130

uniform vec3 uLight, uColor;
uniform sampler2D uTexUnit0; 

varying vec3 vNormal;
varying vec3 vPosition;

varying vec2 vTexCoord0;

void main() {
  vec3 tolight = normalize(uLight - vPosition);
  vec3 normal = normalize(vNormal);

  float diffuse = max(0.2, dot(normal, tolight));
  vec3 intensity = uColor * diffuse;

  vec4 texColor0 = texture2D(uTexUnit0, vTexCoord0); // get texture color 
  texColor0.x *= diffuse;
  texColor0.y *= diffuse;
  texColor0.z *= diffuse;

  gl_FragColor = 0.3 * vec4(intensity, 1.0) + 0.7 * texColor0;
}
